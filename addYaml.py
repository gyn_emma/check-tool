import os
import argparse
rep="github"
control= " NA"
repo= " NA"
prefix= " NA"
seperator= " NA"

def find_target(path):
    global control
    global repo
    global prefix
    global seperator
    file=open(path,'r')
    line=0
    pkg=""
    for eachline in file:
        line=line+1
        if(eachline.find("%global") !=-1):
            str=eachline.split(" ")
            if str[1]=="gem_name":
                pkg=str[2].strip('\n')
        if(eachline.find("URL") != -1):
            if (eachline.find("github") != -1):
                control = " "+"github"
                print(control)
                index = eachline.find("github.com/")
                if index !=-1:
                    index=index+11
                    repo = " "+eachline[index:len(eachline)-1]
                    prefix = " "+"\"v\""
                    seperator = " "+"\".\""
                    print(repo)

            elif (eachline.find("rubygems") != -1):
                control = " " + "rubygem"
                index = eachline.find("gems/")
                if index !=-1:
                    repo = " "+pkg
                    prefix = " "+"\"\""
                    seperator = " "+"\".\""
                    print(repo)
    file.close()

def creatYaml(path):
    new_path=path.strip(".spec")+".yaml"
    print(new_path)
    file=open(new_path,'w')
    line="version_control:"+control+"\n"
    file.write(line)
    line="src_repo:"+repo+"\n"
    file.write(line)
    line="tag_prefix:"+prefix+"\n"
    file.write(line)
    line = "seperator:" + seperator
    file.write(line)
    file.close()


if __name__ == '__main__':
    parser=argparse.ArgumentParser()
    parser.add_argument('--path','-p',type=str,default="C:/Users/g50016582/Desktop/test_spec")
    parser.add_argument('--dir','-d',type=str,default="C:/Users/g50016582/Desktop/test_spec/1")
    args=parser.parse_args()
    root_path=args.path
    root_dir=args.dir

    list=[]
    if(os.path.exists(root_path)):
        dirs=os.listdir(root_path)
        for dir in dirs:
            m=os.path.join(root_path,dir)
            p=""
            if(os.path.isdir(m)):
                print(m+":addYaml...")
                f_list=os.listdir(m)
                for f in f_list:
                    control = " NA"
                    repo = " NA"
                    prefix = " NA"
                    seperator = " NA"
                    if os.path.splitext(f)[1]=='.spec':
                        print(f)
                        p=os.path.join(m, f)
                        find_target(p)
                        creatYaml(p)

    if(os.path.exists(dir)):
        m = os.path.join(root_path, dir)
        p=""
        if (os.path.isdir(m)):
            print(m+":addYaml...")
            f_list = os.listdir(m)
            for f in f_list:
                if os.path.splitext(f)[1] == '.spec':
                    p = os.path.join(m, f)
                    find_target(p)
        creatYaml(p)
