import os
import argparse
pkg_name = ["javadoc", "doc"]
pkg_line = ["%files", "%package","%description"]
insert_line=["Provides", "Obsoletes"]
rep = "help"

def find_target(path, target):
    file_old=open(path,'r')
    new_path=path+"new"
    file_new=open(new_path,'w')
    line=0
    rep=0
    for eachline in file_old:
        line=line+1
        new_line = eachline
        for f in pkg_line:
            if (eachline.find(f) != -1):
                for k in pkg_name:
                    if (eachline.find(k) != -1):
                        if f == '%description':
                            for insert in insert_line:
                                if(insert=="Provides"):
                                    add=insert+":"+(21 - len(insert)-1)*" "+"%{name}-"+k+" = %{version}-%{release}"+"\n"
                                    file_new.write(add)
                                if (insert == "Obsoletes"):
                                    add = insert + ":" + (21 - len(insert) - 1) * " " + "%{name}-" + k + " < %{version}-%{release}" + "\n"
                                    file_new.write(add)
                        print(eachline)
                        new_line=eachline.replace(k,target)
                        print(new_line)
                        break
        file_new.write(new_line)
    file_old.close()
    file_new.close()



if __name__ == '__main__':
    parser=argparse.ArgumentParser()
    parser.add_argument('--path','-p',type=str,default="C:/Users/g50016582/Desktop/test_spec")
    parser.add_argument('--dir','-d',type=str,default="C:/Users/g50016582/Desktop/test_spec/1")
    args=parser.parse_args()
    root_path=args.path
    root_dir=args.dir

    list=[]
    if(os.path.exists(root_path)):
        dirs=os.listdir(root_path)
        for dir in dirs:
            m=os.path.join(root_path,dir)
            if(os.path.isdir(m)):
                print(m)
                f_list=os.listdir(m)
                for f in f_list:
                    if os.path.splitext(f)[1]=='.spec':
                        find_target(os.path.join(m, f), rep)

    if(os.path.exists(dir)):
        m = os.path.join(root_path, dir)
        if (os.path.isdir(m)):
            print(m)
            f_list = os.listdir(m)
            for f in f_list:
                if os.path.splitext(f)[1] == '.spec':
                    find_target(os.path.join(m, f), rep)